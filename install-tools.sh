#!/bin/bash

echo 'installing tools'

# installing KinD (kubernetes in docker)

curl -Lo /tmp/kind https://kind.sigs.k8s.io/dl/latest/kind-linux-amd64
chmod +x /tmp/kind
sudo mv /tmp/kind /usr/local/bin/kind
echo 'kind installed'
kind version

# installing kubectl

curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
chmod +x /tmp/kubectl
sudo mv /tmp/kubectl /usr/local/bin/kubectl
echo 'kubectl installed'
kubectl version --client

# installing kustomize

opsys=linux  # or darwin, or windows
curl https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh | bash
chmod u+x kustomize
sudo mv ./kustomize /usr/local/bin/kustomize
echo 'kustomize installed to bin directory'

echo 'tool installation complete'
